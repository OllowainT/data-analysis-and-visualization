# @author Markus Betz
# run local: Rscript filename
# install packages usw. R.exe --> install.packages("name")

# hinweise: erste beide Zeilen gelöscht, Umlaute ersetzt

#load packages
require(plyr)
require(sqldf)
library(ggplot2)
library(ggpubr) #correlation
library(corrplot)#correlation matrix
library(gridExtra)
library(sp)
library(rgdal)
library(RColorBrewer)
library(maps)
library(mapdata)
library(shapefiles)
library(maptools)
library(raster)
library(tidyverse)
library(stringr)
library(viridis)
library(shiny)
library(shinydashboard)

sink('analysis-output.txt') #write a log

#1 data preparation
gAdsWDR <- read.csv2("Rohdaten\\csvWoche-Tag-Region.csv", header = TRUE, sep = ";", dec = ",", fill = TRUE,encoding="UTF-8")
temp_min <- read.csv("Rohdaten\\min_temp\\TNK_MN004_min.txt", header = TRUE, sep = ";", dec = ".", fill = TRUE,encoding="UTF-8")
temp_max <- read.csv("Rohdaten\\max_temp\\TXK_MN004_max.txt", header = TRUE, sep = ";", dec = ".", fill = TRUE,encoding="UTF-8")

#get type for column (factor is char)
lapply(gAdsWDR, class)
lapply(temp_min, class)
lapply(temp_max, class)
#temp_min

# 2 change header name
cat("changing header names gAds...\n")
cat("old header names: ", names(gAdsWDR),"\n")
colnames(gAdsWDR)[colnames(gAdsWDR)=="Woche"] <- "week_of"
colnames(gAdsWDR)[colnames(gAdsWDR)=="Tag"] <- "day"
colnames(gAdsWDR)[colnames(gAdsWDR)=="Region..geografisches.Gebiet."] <- "state"
colnames(gAdsWDR)[colnames(gAdsWDR)=="Region..geografisches.Gebiet."] <- "state" #??
colnames(gAdsWDR)[colnames(gAdsWDR)=="W.e4.hrung"] <- "currency"
colnames(gAdsWDR)[colnames(gAdsWDR)=="Klicks"] <- "clicks"
colnames(gAdsWDR)[colnames(gAdsWDR)=="Impressionen"] <- "impressions"
colnames(gAdsWDR)[colnames(gAdsWDR)=="CTR"] <- "ctr"
colnames(gAdsWDR)[colnames(gAdsWDR)=="Durchschn..CPC"] <- "average_cpc"
colnames(gAdsWDR)[colnames(gAdsWDR)=="Kosten"] <- "costs"
colnames(gAdsWDR)[colnames(gAdsWDR)=="Durchschn..Position"] <- "average_position"

cat("old header names temp_min: ", names(temp_min),"\n")
colnames(temp_min)[colnames(temp_min)=="ZEITSTEMPEL"] <- "date"
colnames(temp_min)[colnames(temp_min)=="WERT"] <- "temp_min"
colnames(temp_min)[colnames(temp_min)=="STATION_ID"] <- "state"
colnames(temp_min)[colnames(temp_min)=="ZEITSTEMPEL"] <- "date"

cat("old header names temp_max: ", names(temp_max),"\n")
colnames(temp_max)[colnames(temp_max)=="ZEITSTEMPEL"] <- "date"
colnames(temp_max)[colnames(temp_max)=="WERT"] <- "temp_max"
colnames(temp_max)[colnames(temp_max)=="STATION_ID"] <- "state" # the station id is an indicator for state, because for every state there is one station --> see step 4

cat("changed header names gAds: ", names(gAdsWDR),"\n") # check for changed names
cat("changed header names temp_min: ", names(temp_min),"\n") # check for changed names
cat("changed header names temp_max: ", names(temp_max),"\n") # check for changed names

#3
gAdsWDR$week_of <- substr(gAdsWDR$week_of, 10, 20) #delete "Woche von "
#gAdsWDR$ctr <- unlist(strsplit(as.character(gAdsWDR$ctr), "+")) #delete " %"

gAdsWDR$ctr <- as.character(gAdsWDR$ctr) # change ctr to chars
gAdsWDR$state <- as.character(gAdsWDR$state) # change ctr to chars
Encoding(gAdsWDR$ctr) <- "latin1"  #change utf to latin for easier substringing
Encoding(gAdsWDR$state) <- "latin1"

#gAdsWDR$ctr <- gsub("^[0-9]*,[0-9]*","", as.character(gAdsWDR$ctr))
gAdsWDR$ctr <- gsub("\\s%","", as.character(gAdsWDR$ctr)) # remove %
gAdsWDR$ctr <- gsub(",",".", as.character(gAdsWDR$ctr)) # chang comma to dot
gAdsWDR$ctr <- as.numeric(gAdsWDR$ctr) # change type to numeric


#4 change formats
#change station number to state by mapping --> see next step
temp_min$state <- revalue(as.factor(temp_min$state), c("1051"="DE-SN", "3379"="DE-BY", "3137"="DE-RP", "1270"="DE-TH", "3987"="DE-BB", "691"="DE-HB", "3126"="DE-ST", "1975"="DE-HH", "4928"="DE-BW",
 "2564"="DE-SH", "427"="DE-BE", "2014"="DE-NI", "6217"="DE-SL", "5541"="DE-HE", "1078"="DE-NW", "4625"="DE-MV"))

 temp_max$state <- revalue(as.factor(temp_max$state), c("1051"="DE-SN", "3379"="DE-BY", "3137"="DE-RP", "1270"="DE-TH", "3987"="DE-BB", "691"="DE-HB", "3126"="DE-ST", "1975"="DE-HH", "4928"="DE-BW",
  "2564"="DE-SH", "427"="DE-BE", "2014"="DE-NI", "6217"="DE-SL", "5541"="DE-HE", "1078"="DE-NW", "4625"="DE-MV"))


#change state names to iso format
gAdsWDR$state <- revalue(gAdsWDR$state, c("Bayern"="DE-BY", "Brandenburg"="DE-BB", "Berlin"="DE-BE", "Bremen"="DE-HB"
, "Hamburg"="DE-HH", "Hessen"="DE-HE", "Mecklenburg-Vorpommern"="DE-MV", "Niedersachsen"="DE-NI", "Nordrhein-Westfalen"="DE-NW", "Rheinland-Pfalz"="DE-RP"
, "Saarland"="DE-SL", "Sachsen"="DE-SN", "Sachsen-Anhalt"="DE-ST", "Schleswig-Holstein"="DE-SH", "Keine Angabe"="DE", "Thueringen"="DE-TH", "Baden-Wuerttemberg"="DE-BW"))

#gAdsWDR$average_temp <- (gAdsWDR$temp_min + gAdsWDR$temp_max) / 2

gAdsWDR$week_of <- as.Date(gAdsWDR$week_of,format = "%d.%m.%Y") # change factor (string/char) to Date
gAdsWDR$day <- as.Date(gAdsWDR$day,format = "%d.%m.%Y") # change factor (string/char) to Date

temp_max$date <- as.Date(as.factor(temp_max$date), format = "%Y%m%d")
temp_min$date <- as.Date(as.factor(temp_min$date), format = "%Y%m%d")
#delete not needed columns
temp_max$PRODUKT_CODE <- NULL
temp_max$QN <- NULL
temp_max$QB <- NULL
temp_min$PRODUKT_CODE <- NULL
temp_min$QN <- NULL
temp_min$QB <- NULL

#add temp by key day and state to gAdsWDR
gAdsWDR <- merge(gAdsWDR, temp_min, by.x = c("day", "state"), by.y = c("date", "state"), all.x = TRUE, all.y = FALSE)
gAdsWDR <- merge(gAdsWDR, temp_max, by.x = c("day", "state"), by.y = c("date", "state"), all.x = TRUE, all.y = FALSE)

#order by week_of
#gAdsWDR$week_of <- order(gAdsWDR, week_of)
gAdsWDR <- gAdsWDR[order(as.Date(gAdsWDR$week_of,format="%d-%m-%Y")),,drop=FALSE]
# add average temp
temp_average <- (gAdsWDR$temp_max + gAdsWDR$temp_min) /2
gAdsWDR$temp_average <- temp_average
#add season
season <- c(
  "01" = "Winter", "02" = "Winter",
  "03" = "Spring", "04" = "Spring", "05" = "Spring",
  "06" = "Summer", "07" = "Summer", "08" = "Summer",
  "09" = "Fall", "10" = "Fall", "11" = "Fall",
  "12" = "Winter"
)
gAdsWDR$season <- season[format(gAdsWDR$day, "%m")]

#add month - generated by day
gAdsWDR$month <- format(gAdsWDR$day, "%m")

lapply(gAdsWDR, class) # check for types

print(head(gAdsWDR, 26))
#print(head(temp_min, 10))
#print(head(temp_max, 10))




#get our diskreptive variance
getVariance <- function(field){
  v = var(field, na.rm=TRUE) * (length(field)-1) / length(field)
  value = v
}

#get our Standardabweichung (nicht die inferenzstatistische)
getStdAbw <- function(field){
  v = sqrt(var(field, na.rm=TRUE) * (length(field)-1) / length(field))
  value = v
}


#End of Data preperation - lets get some results


cat("Durchschnittliche Kosten pro Klick: ",mean(gAdsWDR$average_cpc), "\n")
cat("Median Kosten pro Klick: ",median(gAdsWDR$average_cpc), "\n")
cat("Diskriptive Varianz Kosten pro Klick: ",getVariance(gAdsWDR$average_cpc), "\n")
cat("Standardabweichung Kosten pro Klick: ",getStdAbw(gAdsWDR$average_cpc), "\n")
#plot cpc overall data
png(filename="cpc_overall.png", width=512, height=1024, pointsize = 20 )
boxplot(gAdsWDR$average_cpc, ylab="CPC", main ="CPC overall")
dev.off()


cat("Durchschnittliche Position: ",mean(gAdsWDR$average_position), "\n")
cat("Median Position: ",median(gAdsWDR$average_position), "\n")
cat("Diskriptive Varianz  Position: ",getVariance(gAdsWDR$average_position), "\n")
cat("Standardabweichung Position: ",getStdAbw(gAdsWDR$average_position), "\n")

# #plot average position overall data
# png(filename="position_overall.png", width=512, height=1024, pointsize = 20 )
# boxplot(gAdsWDR$average_position, ylab="Position", main ="Position overall")
# dev.off()

generateBoxplot <- function(filename, columnData, xlabel, ylabel, titleName){
  png(filename=paste(filename, "boxplot.png"), width=512, height=1024, pointsize = 20 )
  boxplot(columnData, xlab=xlabel, ylab=ylabel, main = titleName)
  dev.off()
}

generateBoxplot1 <- function(filename, columnData, xlabel, ylabel, titleName){ #for shiny
  boxplot(columnData, xlab=xlabel, ylab=ylabel, main=titleName)
}


cat("Durchschnittliche Temperatur: ",mean(gAdsWDR$temp_average, na.rm=TRUE), "\n")
cat("Median Temperatur: ",median(gAdsWDR$temp_average, na.rm=TRUE), "\n")
cat("Diskriptive Varianz Temperatur: ",getVariance(gAdsWDR$temp_average), "\n")
cat("Standardabweichung Temperatur: ",getStdAbw(gAdsWDR$temp_average), "\n")

cat("Durchschnittliche Temperatur min: ",mean(gAdsWDR$temp_min, na.rm=TRUE), "\n")
cat("Median Temperatur min: ",median(gAdsWDR$temp_min, na.rm=TRUE), "\n")
cat("Diskriptive Varianz Temperatur min: ",getVariance(gAdsWDR$temp_min), "\n")
cat("Standardabweichung Temperatur min: ",getStdAbw(gAdsWDR$temp_min), "\n")

cat("Durchschnittliche Temperatur max: ",mean(gAdsWDR$temp_max, na.rm=TRUE), "\n")
cat("Median Temperatur max: ",median(gAdsWDR$temp_max, na.rm=TRUE), "\n")
cat("Diskriptive Varianz Temperatur max: ",getVariance(gAdsWDR$temp_max), "\n")
cat("Standardabweichung Temperatur max: ",getStdAbw(gAdsWDR$temp_max), "\n")

#get everage temps
generateBoxplot("tempAverageOverall", gAdsWDR$temp_average, "average temperature", "temperature", "Durchschnittstemperatur Overall")
generateBoxplot("tempAverageMinOverall", gAdsWDR$temp_min, "average min temperature", "temperature", "Durchschnittstemperatur min Overall")
generateBoxplot("tempAverageMaxOverall", gAdsWDR$temp_max, "average max temperature", "temperature", "Durchschnittstemperatur max Overall")
#dim(gAdsWDR)

cat("Anzahl Gesamt Impressionen: ",sum(gAdsWDR$impressions), "\n")
cat("Durchschnittliche Impressionen: ",mean(gAdsWDR$impressions), "\n")
cat("Diskreptive Varianz Impressionen: ",getVariance(gAdsWDR$impressions), "\n")
cat("Standardabweichung Impressionen: ",getStdAbw(gAdsWDR$impressions), "\n")
impressions_per_week_of <- sqldf("SELECT week_of, SUM(impressions) as total FROM gAdsWDR GROUP BY week_of ORDER BY week_of")
#lapply(impressions_per_week_of, class)
png(filename="impressions_per_week_of.png", width=2048, height=1024, pointsize = 20 )
plot(total ~ week_of, impressions_per_week_of, type = "l", axes=FALSE, xlab="months", ylab="impressions", main ="Impressions per week", col="blue")
abline(h=mean(impressions_per_week_of$total), col="red")
axis.Date(1, at=seq(min(impressions_per_week_of$week_of), max(impressions_per_week_of$week_of), by="7 day"), format="%Y-%m-%d") #awesome
axis(side=2, at=seq(0, 4000, by=500))
meanforimpression <- format(round(mean(impressions_per_week_of$total), 2), nsmall = 2)
legend(as.Date("2016-09-26"),2500, legend=c("impressions",meanforimpression), col=c("blue", "red"), lty=1:2, cex=0.8, fill=c("blue", "red"))
box()
dev.off()


cat("Anzahl Gesamt Klicks: ",sum(gAdsWDR$clicks), "\n")
cat("Durchschnittliche Klicks: ",mean(gAdsWDR$clicks), "\n")
cat("Diskreptive Varianz Klicks: ",getVariance(gAdsWDR$clicks), "\n")
cat("Standardabweichung Klicks: ",getStdAbw(gAdsWDR$clicks), "\n")


# weekly graphs
clicks_per_week_of <- sqldf("SELECT week_of, SUM(clicks) as totalClicks FROM gAdsWDR GROUP BY week_of ORDER BY week_of")
temp_min_per_week_of<- sqldf("SELECT week_of, AVG(temp_min) as average_temp_min FROM gAdsWDR GROUP BY week_of ORDER BY week_of")
temp_max_per_week_of<- sqldf("SELECT week_of, AVG(temp_max) as average_temp_max FROM gAdsWDR GROUP BY week_of ORDER BY week_of")

png(filename="clicks_per_week_of.png", width=2048, height=1024, pointsize = 20 )
plot(totalClicks ~ week_of, clicks_per_week_of, type = "l", axes=FALSE, xlab="week of", ylab="clicks", main ="Clicks per week", col="blue")
abline(h=mean(clicks_per_week_of$totalClicks), col="red")
axis.Date(1, at=seq(min(clicks_per_week_of$week_of), max(clicks_per_week_of$week_of), by="7 day"), format="%Y-%m-%d") #awesome
axis(side=2, at=seq(0, 100, by=10))
meanforclicks <- format(round(mean(clicks_per_week_of$totalClicks), 2), nsmall = 2)
legend(as.Date("2016-09-26"),80, legend=c("clicks",meanforclicks), col=c("blue", "red"), lty=1:2, cex=0.8, fill=c("blue", "red"))
box()
dev.off()


png(filename="temp_min_per_week_of.png", width=2048, height=1024, pointsize = 20 )
plot(average_temp_min ~ week_of, temp_min_per_week_of, type = "l", axes=FALSE, xlab="week of", ylab="average temp min", main ="Average Temp min per week", col="blue")
abline(h=mean(temp_min_per_week_of$average_temp_min), col="red")
axis.Date(1, at=seq(min(temp_min_per_week_of$week_of), max(temp_min_per_week_of$week_of), by="7 day"), format="%Y-%m-%d")
axis(side=2, at=seq(0, 100, by=10))
meanfortemp_min <- format(round(mean(temp_min_per_week_of$average_temp_min), 2), nsmall = 2)
legend(as.Date("2016-09-26"),15, legend=c("average temp min",meanfortemp_min), col=c("blue", "red"), lty=1:2, cex=0.8, fill=c("blue", "red"))
box()
dev.off()

png(filename="temp_max_per_week_of.png", width=2048, height=1024, pointsize = 20 )
plot(average_temp_max ~ week_of, temp_max_per_week_of, type = "l", axes=FALSE, xlab="week of", ylab="average temp max", main ="Average Temp max per week", col="blue")
abline(h=mean(temp_max_per_week_of$average_temp_max), col="red")
axis.Date(1, at=seq(min(temp_max_per_week_of$week_of), max(temp_max_per_week_of$week_of), by="7 day"), format="%Y-%m-%d")
axis(side=2, at=seq(0, 100, by=10))
meanfortemp_max <- format(round(mean(temp_max_per_week_of$average_temp_max), 2), nsmall = 2)
legend(as.Date("2016-09-26"),22, legend=c("average temp max",meanfortemp_max), col=c("blue", "red"), lty=1:2, cex=0.8, fill=c("blue", "red"))
box()
dev.off()




###################################################
# linear regrssion:
###################################################

# start Korrelation
# hilfreich(2.2): https://www.uni-kassel.de/fb07/fileadmin/datas/fb07/5-Institute/IVWL/Kosfeld/lehre/multivariate/Multivariate_Statistik_mit_R.pdf
clicks_impressions_per_week_of <- sqldf("SELECT week_of, SUM(clicks) as totalClicks, SUM(impressions) as total FROM gAdsWDR GROUP BY week_of ORDER BY week_of")
#clicks_impressions_per_week_of_under2000 <- sqldf("SELECT * FROM clicks_impressions_per_week_of WHERE total < 2000")
clicks_impressions_per_week_of_under2000 <- sqldf("SELECT week_of, SUM(clicks) as totalClicks, SUM(impressions) as total FROM gAdsWDR GROUP BY week_of HAVING total < 2000 ORDER BY week_of ")
#head(clicks_impressions_per_week_of, 10)
cat("pearson: ",pearsonsKorr_Impressions_Clicks <- cor(clicks_impressions_per_week_of$total, clicks_impressions_per_week_of$totalClicks, method="pearson"), "\n")
#unfug cat("spearman:  ",pearsonsKorr_Impressions_Clicks <- cor(clicks_impressions_per_week_of$total, clicks_impressions_per_week_of$totalClicks, method="spearman"), "\n")
png(filename="corrrelation_pearson_click_impresisons_per_week_of.png", width=2048, height=1024, pointsize = 20 )
ggscatter(clicks_impressions_per_week_of, x = "total", y = "totalClicks",
          add = "reg.line", ellipse = TRUE,
          cor.coef = TRUE, cor.method = "pearson",
          xlab = "impressions", ylab = "clicks")
dev.off()
cat("korrelation_cost_impression:")
cor.test(clicks_impressions_per_week_of$total, clicks_impressions_per_week_of$totalClicks)
cat("#############################\n")



png(filename="corrrelation_pearson_click_impresisons_per_week_of_under2000.png", width=2048, height=1024, pointsize = 20 )
ggscatter(clicks_impressions_per_week_of_under2000, x = "total", y = "totalClicks",
          add = "reg.line", ellipse = TRUE,
          cor.coef = TRUE, cor.method = "pearson",
          xlab = "impressions", ylab = "clicks")
dev.off()
cat("korrelation_cost_impression under 2000:")
cor.test(clicks_impressions_per_week_of_under2000$total, clicks_impressions_per_week_of_under2000$totalClicks)
cat("#############################\n")
lm(formula= clicks_impressions_per_week_of_under2000$total ~ clicks_impressions_per_week_of_under2000$totalClicks)


###################################################
# correlation matrix:
###################################################
#group data by weeks:
weeklygAdsWDR <- sqldf("SELECT week_of, SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position_week, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE clicks >=1
 GROUP BY week_of ORDER BY week_of")
weeklyCTR <- (weeklygAdsWDR$totalClicks / weeklygAdsWDR$totalImpressions)*100 # calculate ctr
weeklyAverageCosts <- weeklygAdsWDR$costs/weeklygAdsWDR$totalClicks # calculate costs average per week
weeklygAdsWDR$weeklyCTR <- weeklyCTR
weeklygAdsWDR$average_costs <- weeklyAverageCosts
# dim(weeklygAdsWDR)
# head(weeklygAdsWDR, 10)


#data by season
#head(gAdsWDR, 10)
seasongAdsWDR <- sqldf("SELECT season, SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE clicks >=1
 GROUP BY season ORDER BY season")
seasonCTR <- (seasongAdsWDR$totalClicks / seasongAdsWDR$totalImpressions)*100 # calculate ctr
seasongAdsWDR$seasonCTR <- seasonCTR
seasonAverageCosts <- seasongAdsWDR$costs/seasongAdsWDR$totalClicks
seasongAdsWDR$average_costs <- seasonAverageCosts

#data by day
#head(gAdsWDR, 10)
daygAdsWDR <- sqldf("SELECT day, SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE clicks >=1
 GROUP BY day ORDER BY day")
dayCTR <- (daygAdsWDR$totalClicks / daygAdsWDR$totalImpressions)*100 # calculate ctr
daygAdsWDR$dayCTR <- dayCTR
dayAverageCosts <- daygAdsWDR$costs/daygAdsWDR$totalClicks
daygAdsWDR$average_costs <- dayAverageCosts

#data by month
#head(gAdsWDR, 10)
monthgAdsWDR <- sqldf("SELECT month, SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE clicks >=1
 GROUP BY month ORDER BY month")
monthCTR <- (monthgAdsWDR$totalClicks / monthgAdsWDR$totalImpressions)*100 # calculate ctr
monthgAdsWDR$monthCTR <- monthCTR
monthAverageCosts <- monthgAdsWDR$costs/monthgAdsWDR$totalClicks
monthgAdsWDR$average_costs <- monthAverageCosts

#by overall
cat("Anzahl Zeilen GoogleAds Datensatz: ", length(gAdsWDR[,1]), "\n") #dim() wuerde auch gehen
cat("Anzahl Spalten GoogleAds Datensatz: ", length(gAdsWDR), "\n")
cat("Durchschnittliche Kosten  overall: ",mean(gAdsWDR$costs), "\n")
cat("Median Kosten overall: ",median(gAdsWDR$costs), "\n")

#by day
cat("Anzahl Zeilen GoogleAds Datensatz: ", length(daygAdsWDR[,1]), "\n") #dim() wuerde auch gehen
cat("Anzahl Spalten GoogleAds Datensatz: ", length(daygAdsWDR), "\n")
cat("Durchschnittliche Kosten am Tag: ",mean(daygAdsWDR$costs), "\n")
cat("Median Kosten  am Tag: ",median(daygAdsWDR$costs), "\n")

#by week
cat("Anzahl Zeilen GoogleAds Datensatz: ", length(weeklygAdsWDR[,1]), "\n") #dim() wuerde auch gehen
cat("Anzahl Spalten GoogleAds Datensatz: ", length(weeklygAdsWDR), "\n")
cat("Durchschnittliche Kosten pro Woche: ",mean(weeklygAdsWDR$costs), "\n")
cat("Median Kosten in der Woche: ",median(weeklygAdsWDR$costs), "\n")

#by month
cat("Anzahl Zeilen GoogleAds Datensatz: ", length(monthgAdsWDR[,1]), "\n") #dim() wuerde auch gehen
cat("Anzahl Spalten GoogleAds Datensatz: ", length(monthgAdsWDR), "\n")
cat("Durchschnittliche Kosten pro Monat: ",mean(monthgAdsWDR$costs), "\n")# ACHTUNG! Über beide Jahre!
cat("Median Kosten im Monat: ",median(monthgAdsWDR$costs), "\n")

#by season
cat("Anzahl Zeilen GoogleAds Datensatz: ", length(seasongAdsWDR[,1]), "\n") #dim() wuerde auch gehen
cat("Anzahl Spalten GoogleAds Datensatz: ", length(seasongAdsWDR), "\n")
cat("Durchschnittliche Kosten pro Jahreszeit: ",mean(seasongAdsWDR$costs), "\n")# ACHTUNG! Über beide Jahre!
cat("Median Kosten pro Jahreszeit: ",median(seasongAdsWDR$costs), "\n")

#plot table as exaple in presenatition
png(filename="weeklygAdsWDR.png", width=1024, height=768, pointsize = 20 )
grid.table(head(weeklygAdsWDR, 5))
dev.off()

png(filename="daygAdsWDR.png", width=1024, height=768, pointsize = 20 )
grid.table(head(daygAdsWDR, 5))
dev.off()

png(filename="seasongAdsWDR.png", width=1024, height=768, pointsize = 20 )
grid.table(head(seasongAdsWDR, 5))
dev.off()

png(filename="monthgAdsWDR.png", width=1024, height=768, pointsize = 20 )
grid.table(head(monthgAdsWDR, 5))
dev.off()

#delete Char Columns, to correlate
weeklygAdsWDR$week_of <- NULL
daygAdsWDR$day <- NULL
seasongAdsWDR$season <- NULL
monthgAdsWDR$month <- NULL

#shiny method for correlation
generateCorrelationMatrix <- function (value){
  matrix <- cor(value)
  colMatrix <- colorRampPalette(c("#BB4444", "#EE9988", "#FFFFFF", "#77AADD", "#4477AA"))
  corrplot(matrix, method="circle", type="upper", col=colMatrix(200), addCoef.col = "black", # Add coefficient of correlation
           tl.col="black", tl.srt=45,)
}


#generate weekly correlation matrix:
weeklygAdsWDR_matrix <- cor(weeklygAdsWDR)
#head(weeklygAdsWDR_matrix, 10)
png(filename="correlation_matrix_weekly.png", width=1024, height=1024, pointsize = 20 )
colMatrix <- colorRampPalette(c("#BB4444", "#EE9988", "#FFFFFF", "#77AADD", "#4477AA"))
corrplot(weeklygAdsWDR_matrix, method="circle", type="upper", col=colMatrix(200), addCoef.col = "black", # Add coefficient of correlation
         tl.col="black", tl.srt=45,)
dev.off()

#generate season correlation matrix:
seasongAdsWDR_matrix <- cor(seasongAdsWDR)
#head(seasongAdsWDR_matrix, 10)
png(filename="correlation_matrix_season.png", width=1024, height=1024, pointsize = 20 )
colMatrix <- colorRampPalette(c("#BB4444", "#EE9988", "#FFFFFF", "#77AADD", "#4477AA"))
corrplot(seasongAdsWDR_matrix, method="circle", type="upper", col=colMatrix(200), addCoef.col = "black", # Add coefficient of correlation
         tl.col="black", tl.srt=45,)
dev.off()

#generate day correlation matrix:
daygAdsWDR_matrix <- cor(daygAdsWDR)
#head(daygAdsWDR_matrix, 10)
png(filename="correlation_matrix_day.png", width=1024, height=1024, pointsize = 20 )
colMatrix <- colorRampPalette(c("#BB4444", "#EE9988", "#FFFFFF", "#77AADD", "#4477AA"))
corrplot(daygAdsWDR_matrix, method="circle", type="upper", col=colMatrix(200), addCoef.col = "black", # Add coefficient of correlation
         tl.col="black", tl.srt=45,)
dev.off()

#generate month correlation matrix:
monthgAdsWDR_matrix <- cor(monthgAdsWDR)
#head(monthgAdsWDR_matrix, 10)
png(filename="correlation_matrix_month.png", width=1024, height=1024, pointsize = 20 )
colMatrix <- colorRampPalette(c("#BB4444", "#EE9988", "#FFFFFF", "#77AADD", "#4477AA"))
corrplot(monthgAdsWDR_matrix, method="circle", type="upper", col=colMatrix(200), addCoef.col = "black", # Add coefficient of correlation
         tl.col="black", tl.srt=45,)
dev.off()


cat("Durchschnittliche CTR: ",mean(gAdsWDR$ctr), "\n")
cat("median CTR: ",median(gAdsWDR$ctr, na.rm = TRUE), "\n")
cat("Varianz CTR: ",var(gAdsWDR$ctr), "\n") # thats not the correct variance!!! see next
cat("Diskreptive Varianz  CTR: ",getVariance(gAdsWDR$ctr), "\n")
cat("Standardabweichung CTR: ",getStdAbw(gAdsWDR$ctr), "\n")
generateBoxplot("averageCTR", gAdsWDR$ctr, "average ctr", "ctr", "CTR Overall")

####################################################

# GeoMap
#OverallData Generation
germany<-readOGR("Rohdaten/gadm36_DEU_shp/gadm36_DEU_1.shp", layer="gadm36_DEU_1")
ogrListLayers("Rohdaten/gadm36_DEU_shp/gadm36_DEU_1.shp")
germany <- spTransform(germany,CRS=CRS("+init=epsg:4839"))

#change state to same format
germany$HASC_1 <- revalue(germany$HASC_1, c("DE.SN"="DE-SN", "DE.BY"="DE-BY", "DE.RP"="DE-RP", "DE.TH"="DE-TH", "DE.BR"="DE-BB", "DE.HB"="DE-HB", "DE.ST"="DE-ST", "DE.HH"="DE-HH", "DE.BW"="DE-BW",
 "DE.SH"="DE-SH", "DE.BE"="DE-BE", "DE.NI"="DE-NI", "DE.SL"="DE-SL", "DE.HE"="DE-HE", "DE.NW"="DE-NW", "DE.MV"="DE-MV"))
by <- germany[germany$HASC_1 == "DE-BY",]
groupByStateInfo <- sqldf("SELECT state, SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE impressions >= 1
 GROUP BY state ORDER BY state")
stateCTR <- (groupByStateInfo$totalClicks / groupByStateInfo$totalImpressions)*100 # calculate ctr per state
groupByStateInfo$stateCTR <- stateCTR
#head(groupByStateInfo, 10)

# merge gAdsData with map data by state
germany <- merge(germany, groupByStateInfo, by.x = "HASC_1", by.y = "state", all.x = TRUE, all.y = FALSE)
#head(germany@data, 16)

# generate GEOMAPs

# change position for text at brandenburg cause of overwriting labels
germanyCoords <- coordinates(germany)
germanyCoords[4,2] <- germanyCoords[4,2]-40000

#Allgemein
#function for values
generateMap<- function(columnOfInterest, sortedBy, sequenceLength, titleName, legendTitle, legendPosition){
  png(filename=paste(sortedBy,"germany.png"), width=2048, height=1024, pointsize = 20 )
  classes <- cut(columnOfInterest[],round(seq(min(columnOfInterest, na.rm = TRUE)-0.1, max(columnOfInterest, na.rm = TRUE)+0.1, length.out= sequenceLength ), digits = 2))
  levels(classes) <- as.character(levels(classes))
  colours<-colorRampPalette(c("cadetblue1", "firebrick2"))(sequenceLength) #heat.colors(sequenceLength)
  par(mar = c(0,0,1,9),oma = c(3,0,2,1),
  bg = "white",xpd = TRUE)
  plot(germany,border = "darkgrey", col = colours[classes])

  text(coordinates(germanyCoords), labels =round(columnOfInterest, digits=2), col = "black")
  legend(x = par("usr")[1],y = par("usr")[3], pch = 15, col = colours, legend = levels(classes),ncol = 1, bty = "n", title = legendTitle,
  xjust = legendPosition, yjust = -1.8)
  title(titleName)
  dev.off()
}

generateMapShiny<- function(columnOfInterest, sequenceLength, legendPosition){

  classes <- cut(columnOfInterest[],round(seq(min(columnOfInterest, na.rm = TRUE)-0.1, max(columnOfInterest, na.rm = TRUE)+0.1, length.out= sequenceLength ), digits = 2))
  levels(classes) <- as.character(levels(classes))
  colours<-colorRampPalette(c("cadetblue1", "firebrick2"))(sequenceLength) #heat.colors(sequenceLength)
  par(mar = c(0,0,1,9),oma = c(3,0,2,1),
  bg = "white",xpd = TRUE)
  plot(germany,border = "darkgrey", col = colours[classes])
  text(coordinates(germanyCoords), labels =round(columnOfInterest, digits=2), col = "black")
  legend(x = par("usr")[1],y = par("usr")[3], pch = 15, col = colours, legend = levels(classes),ncol = 1, bty = "n",
  xjust = legendPosition, yjust = -1.8)

}


png(filename="totalCosts_by_state_overall.png", width=2048, height=1024, pointsize = 20 )
classes <- cut(germany$costs[],round(seq(min(germany$costs)-0.1, max(germany$costs)+0.1, length.out= 9 ), digits = 2))#c(400, 300, 200, 150, 100, 70, 40, 20, 0))
levels(classes) <- as.character(levels(classes)) #c("300-400", "200-300", "150-200", "100-150", "70-100", "40-70", "20-40", "unter 20"))
colours<- colorRampPalette(c("cadetblue1", "red"))(9) #heat.colors(9)
par(mar = c(0,0,1,9),oma = c(3,0,2,1),
bg = "white",xpd = TRUE)
plot(germany,border = "darkgrey", col = colours[classes])
#text(coordinates(germany), labels =germany$HASC_1, col = "white")
text(coordinates(germany), labels =germany$costs, col = "black")
legend(x = par("usr")[1],y = par("usr")[3], pch = 15, col = colours, legend = levels(classes),ncol = 1, bty = "n", title = "Gesamtkosten",
xjust = -3.4, yjust = -1.8)
title("Gesamtkosten der Kampagne in Deutschland")
dev.off()

png(filename="average_position_week_by_state_overall.png", width=2048, height=1024, pointsize = 20 )
classes <- cut(germany$average_position[],round(seq(min(germany$average_position)-0.1, max(germany$average_position)+0.1, length.out= 6 ), digits = 2)) # old: cut(germany$average_position[],c(3.8, 3.9, 4, 4.1, 4.2, 4.5))
levels(classes) <- as.character(levels(classes)) #c(,"3.8-3.9", "3.9-4", "4-4.1", "4.1-4.2", ">4.3")
colours<-colorRampPalette(c("cadetblue1", "red"))(6)#heat.colors(6)
par(mar = c(0,0,1,9),oma = c(3,0,2,1),
bg = "white",xpd = TRUE)
plot(germany,border = "darkgrey", col = colours[classes])
text(coordinates(germany), labels =round(germany$average_position, digits=2), col = "black")
legend(x = par("usr")[1],y = par("usr")[3], pch = 15, col = colours, legend = levels(classes),ncol = 1, bty = "n", title = "Durchschnittliche Position",
xjust = -5.1, yjust = -1.8)
title("Durchschnittliche Position der Kampagne in Deutschland")
dev.off()


generateMap(germany$stateCTR, "stateCTR_by_state_overall", 8, "Durchschnittliche CTR per Bundesland", "Durchschnittliche CTR", -5.9)
generateMap(germany$average_temp_max, "average_temp_max_by_state_overall", 8, "Durchschnittliche maximale Temperatur per Bundesland bei Impression", "Durchschnittliche max Temp", -5.0)
generateMap(germany$average_temp_min, "average_temp_min_by_state_overall", 8, "Durchschnittliche minimale Temperatur per Bundesland bei Impression", "Durchschnittliche min Temp", -5.0)
generateMap(germany$temp_average, "average_temp_by_state_overall", 8, "Durchschnittliche Temperatur per Bundesland bei Impression", "Durchschnittliche Temp", -5.7)
generateMap(germany$totalImpressions, "totalImpressions_by_state_overall", 8, "Anzahl Impressionen per Bundesland", "Anzahl Impressionen", -5.2)
generateMap(germany$totalClicks, "totalClicks_by_state_overall", 8, "Anzahl Clicks per Bundesland", "Anzahl Clicks", -3)

#Per Click Data Generation
# new Data for Impressionsmap
germanyForImpr<-readOGR("Rohdaten/gadm36_DEU_shp/gadm36_DEU_1.shp", layer="gadm36_DEU_1")
ogrListLayers("Rohdaten/gadm36_DEU_shp/gadm36_DEU_1.shp")
germanyForImpr <- spTransform(germanyForImpr,CRS=CRS("+init=epsg:4839"))

#change state to same format
germanyForImpr$HASC_1 <- revalue(germanyForImpr$HASC_1, c("DE.SN"="DE-SN", "DE.BY"="DE-BY", "DE.RP"="DE-RP", "DE.TH"="DE-TH", "DE.BR"="DE-BB", "DE.HB"="DE-HB", "DE.ST"="DE-ST", "DE.HH"="DE-HH", "DE.BW"="DE-BW",
 "DE.SH"="DE-SH", "DE.BE"="DE-BE", "DE.NI"="DE-NI", "DE.SL"="DE-SL", "DE.HE"="DE-HE", "DE.NW"="DE-NW", "DE.MV"="DE-MV"))
groupByStateImpr <- sqldf("SELECT state, SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE clicks >= 1
 GROUP BY state ORDER BY state")
stateCTR <- (groupByStateImpr$totalClicks / groupByStateImpr$totalImpressions)*100 # calculate ctr per state
groupByStateImpr$stateCTR <- stateCTR
# merge gAdsData with map data by state
germanyForImpr <- merge(germanyForImpr, groupByStateImpr, by.x = "HASC_1", by.y = "state", all.x = TRUE, all.y = FALSE)
#head(germanyForImpr@data, 16)

generateMap(germanyForImpr$totalImpressions, "totalImpressions_by_state_click", 8, "Anzahl Impressionen mit Klick per Bundesland", "Anzahl Impressionen", -5.2)
generateMap(germanyForImpr$stateCTR, "stateCTR_by_state_click", 8, "Durchschnittliche CTR mit Klick per Bundesland", "Durchschnittliche CTR", -5.9)
generateMap(germanyForImpr$average_temp_max, "average_temp_max_by_state_click", 8, "Durchschnittliche maximale Temperatur per Bundesland bei Impression mit Klick", "Durchschnittliche max Temp", -5.0)
generateMap(germanyForImpr$average_temp_min, "average_temp_min_by_state_click", 8, "Durchschnittliche minimale Temperatur per Bundesland bei Impression mit Klick", "Durchschnittliche min Temp", -5.0)
generateMap(germanyForImpr$temp_average, "average_temp_by_state_click", 8, "Durchschnittliche Temperatur per Bundesland bei Impression mit Klick", "Durchschnittliche Temp", -5.7)
generateMap(germanyForImpr$totalClicks, "totalClicks_by_state_click", 8, "Anzahl Clicks per Bundesland", "Anzahl Clicks", -3)


#Time to get extreme temp days!
#for high temp
germanyForHighTemp<-readOGR("Rohdaten/gadm36_DEU_shp/gadm36_DEU_1.shp", layer="gadm36_DEU_1")
ogrListLayers("Rohdaten/gadm36_DEU_shp/gadm36_DEU_1.shp")
germanyForHighTemp <- spTransform(germanyForHighTemp,CRS=CRS("+init=epsg:4839"))

#hightemp rows --> 116 rows
highTempOverall <- sqldf("SELECT state, clicks, impressions,
 costs, average_position, temp_min, temp_max, temp_average
 FROM gAdsWDR
 WHERE clicks >= 1 AND temp_max > 28")
 #head(highTempOverall, 10)
 #dim(highTempOverall)

# same like above but group by state
germanyForHighTemp$HASC_1 <- revalue(germanyForHighTemp$HASC_1, c("DE.SN"="DE-SN", "DE.BY"="DE-BY", "DE.RP"="DE-RP", "DE.TH"="DE-TH", "DE.BR"="DE-BB", "DE.HB"="DE-HB", "DE.ST"="DE-ST", "DE.HH"="DE-HH", "DE.BW"="DE-BW",
 "DE.SH"="DE-SH", "DE.BE"="DE-BE", "DE.NI"="DE-NI", "DE.SL"="DE-SL", "DE.HE"="DE-HE", "DE.NW"="DE-NW", "DE.MV"="DE-MV"))
groupByStateHighTemp <- sqldf("SELECT state, SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE clicks >= 1 AND temp_max > 28
 GROUP BY state ORDER BY state")


stateCTR <- (groupByStateHighTemp$totalClicks / groupByStateHighTemp$totalImpressions)*100 # calculate ctr per state
groupByStateHighTemp$stateCTR <- stateCTR
# merge gAdsData with map data by state
germanyForHighTemp <- merge(germanyForHighTemp, groupByStateHighTemp, by.x = "HASC_1", by.y = "state", all.x = TRUE, all.y = FALSE)
#germanyForHighTemp <- sqldf("DELETE FROM germanyForHighTemp WHERE average_temp_max IS NULL")
#head(germanyForHighTemp@data, 20)
generateMap(germanyForHighTemp$stateCTR, "stateCTR_by_state_highTemp", 8, "Durchschnittliche CTR bei Temp > 28", "Durchschnittliche CTR", -5.9)


# for low temp
germanyForLowTemp<-readOGR("Rohdaten/gadm36_DEU_shp/gadm36_DEU_1.shp", layer="gadm36_DEU_1")
ogrListLayers("Rohdaten/gadm36_DEU_shp/gadm36_DEU_1.shp")
germanyForLowTemp <- spTransform(germanyForLowTemp,CRS=CRS("+init=epsg:4839"))

#lowtemp rows --> 122 rows
 lowTempOverall <- sqldf("SELECT state, clicks, impressions,
  costs, average_position, temp_min, temp_max, temp_average
  FROM gAdsWDR
  WHERE clicks >= 1 AND temp_min < -5")
  #head(lowTempOverall, 10)
  #dim(lowTempOverall)

germanyForLowTemp$HASC_1 <- revalue(germanyForLowTemp$HASC_1, c("DE.SN"="DE-SN", "DE.BY"="DE-BY", "DE.RP"="DE-RP", "DE.TH"="DE-TH", "DE.BR"="DE-BB", "DE.HB"="DE-HB", "DE.ST"="DE-ST", "DE.HH"="DE-HH", "DE.BW"="DE-BW",
 "DE.SH"="DE-SH", "DE.BE"="DE-BE", "DE.NI"="DE-NI", "DE.SL"="DE-SL", "DE.HE"="DE-HE", "DE.NW"="DE-NW", "DE.MV"="DE-MV"))
groupByStateLowTemp <- sqldf("SELECT state, SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE clicks >= 1 AND temp_min < -5
 GROUP BY state ORDER BY state")

stateCTR <- (groupByStateLowTemp$totalClicks / groupByStateLowTemp$totalImpressions)*100 # calculate ctr per state
groupByStateLowTemp$stateCTR <- stateCTR
# merge gAdsData with map data by state
germanyForLowTemp <- merge(germanyForLowTemp, groupByStateLowTemp, by.x = "HASC_1", by.y = "state", all.x = TRUE, all.y = FALSE)
#head(germanyForLowTemp@data, 20)

generateMap(germanyForLowTemp$stateCTR, "stateCTR_by_state_lowTemp", 8, "Durchschnittliche CTR bei Temp < -5", "Durchschnittliche CTR", -5.9)

# for mid temp temp
germanyMidTemp<-readOGR("Rohdaten/gadm36_DEU_shp/gadm36_DEU_1.shp", layer="gadm36_DEU_1")
ogrListLayers("Rohdaten/gadm36_DEU_shp/gadm36_DEU_1.shp")
germanyMidTemp <- spTransform(germanyMidTemp,CRS=CRS("+init=epsg:4839"))

#midtemp rows --> 120 rows
 MidTempOverall <- sqldf("SELECT state, clicks, impressions,
  costs, average_position, temp_min, temp_max, temp_average
  FROM gAdsWDR
  WHERE clicks >= 1 AND temp_average BETWEEN 8.7 AND 10")
  #head(MidTempOverall, 10)
  #dim(MidTempOverall)

germanyMidTemp$HASC_1 <- revalue(germanyMidTemp$HASC_1, c("DE.SN"="DE-SN", "DE.BY"="DE-BY", "DE.RP"="DE-RP", "DE.TH"="DE-TH", "DE.BR"="DE-BB", "DE.HB"="DE-HB", "DE.ST"="DE-ST", "DE.HH"="DE-HH", "DE.BW"="DE-BW",
 "DE.SH"="DE-SH", "DE.BE"="DE-BE", "DE.NI"="DE-NI", "DE.SL"="DE-SL", "DE.HE"="DE-HE", "DE.NW"="DE-NW", "DE.MV"="DE-MV"))
groupByStateMidTemp <- sqldf("SELECT state, SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE clicks >= 1 AND temp_average BETWEEN 8.7 AND 10
 GROUP BY state ORDER BY state")

stateCTR <- (groupByStateMidTemp$totalClicks / groupByStateMidTemp$totalImpressions)*100 # calculate ctr per state
groupByStateMidTemp$stateCTR <- stateCTR
# merge gAdsData with map data by state
germanyMidTemp <- merge(germanyMidTemp, groupByStateMidTemp, by.x = "HASC_1", by.y = "state", all.x = TRUE, all.y = FALSE)
#head(germanyMidTemp@data, 20)

generateMap(germanyMidTemp$stateCTR, "stateCTR_by_state_midTemp", 8, "Durchschnittliche CTR bei Temp ~ 9", "Durchschnittliche CTR", -5.9)

################################
# for temps not grouped by state
 MidTempALL <- sqldf("SELECT SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
  SUM(costs), AVG(average_position), AVG(temp_min), AVG(temp_max), AVG(temp_average)
  FROM gAdsWDR
  WHERE clicks >= 1 AND temp_average BETWEEN 8.7 AND 10")
midCTR <- (MidTempALL$totalClicks / MidTempALL$totalImpressions)*100 # calculate ctr per state
MidTempALL$midCTR <- midCTR
#head(MidTempALL, 10)
#dim(MidTempALL)

LowTempALL <- sqldf("SELECT SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs), AVG(average_position), AVG(temp_min), AVG(temp_max), AVG(temp_average)
 FROM gAdsWDR
 WHERE clicks >= 1 AND temp_min < -5")
lowCTR <- (LowTempALL$totalClicks / LowTempALL$totalImpressions)*100 # calculate ctr per state
LowTempALL$lowCTR <- lowCTR
#head(LowTempALL, 10)
#dim(LowTempALL)

highTempALL <- sqldf("SELECT SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs), AVG(average_position), AVG(temp_min), AVG(temp_max), AVG(temp_average)
 FROM gAdsWDR
WHERE clicks >= 1 AND temp_max > 29")
highCTR <- (highTempALL$totalClicks / highTempALL$totalImpressions)*100 # calculate ctr per state
highTempALL$highCTR <- highCTR
#head(highTempALL, 10)
#dim(highTempALL)

TempALL <- sqldf("SELECT SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs), AVG(average_position), AVG(temp_min), AVG(temp_max), AVG(temp_average)
 FROM gAdsWDR
WHERE clicks >= 1")
allCTR <- (TempALL$totalClicks / TempALL$totalImpressions)*100 # calculate ctr per state
TempALL$allCTR <- allCTR
#head(TempALL, 10)
#dim(TempALL)


png(filename="ctrByTemp.png", width=768, height=1024, pointsize = 20 )
tempCTRs <- c(TempALL$allCTR, highTempALL$highCTR, LowTempALL$lowCTR, MidTempALL$midCTR)
rownames=c("alle","> 29", "< -5","~9")
colors <- c("orange","tomato2","turquoise3","chocolate4")
ctrPlot<- barplot(tempCTRs, col= colors, border="white", font.axis=2, beside=T,  xlab="Extremtemperaturen", ylab="CTR", font.lab=2)
#par(xpd=T, mar=par()$mar+c(2,2,2,4))
par(mar=c(10.1, 4.1, 4.1, 5))
par(xpd=TRUE)
legend(par("usr")[1],par("usr")[3], pch = 15, col = colors, legend = rownames, xpd=TRUE, ncol = 1, title = "Temperaturen", xjust = -0.5, yjust = -1)
text(ctrPlot,tempCTRs+0.1,labels=as.character(round(tempCTRs, 2)))
title("CTR - Temperaturen extrem")
dev.off()

generateExtremeTemps <- function(changeLengendX, changeLengendY) {
  tempCTRs <- c(TempALL$allCTR, highTempALL$highCTR, LowTempALL$lowCTR, MidTempALL$midCTR)
  rownames=c("alle","> 29", "< -5","~9")
  colors <- c("orange","tomato2","turquoise3","chocolate4")
  ctrPlot<- barplot(tempCTRs, col= colors, border="white", font.axis=2, beside=T,  xlab="Extremtemperaturen", ylab="CTR", font.lab=2)
  par(mar=c(10.1, 4.1, 4.1, 5))
  par(xpd=TRUE)
  legend(par("usr")[1],par("usr")[3], pch = 15, col = colors, legend = rownames, xpd=TRUE, ncol = 1, title = "Temperaturen", xjust = changeLengendX, yjust = changeLengendY)
  text(ctrPlot,tempCTRs+0.1,labels=as.character(round(tempCTRs, 2)))
  title("CTR - Temperaturen extrem")
}


################################
# for temps by month
monthTempALL <- sqldf("SELECT month, SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as monthlyCosts, AVG(average_position), AVG(temp_min), AVG(temp_max), AVG(temp_average) as temp_average
 FROM gAdsWDR
WHERE clicks >= 1
GROUP BY month ORDER BY month")
monthallCTR <- (monthTempALL$totalClicks / monthTempALL$totalImpressions)*100 # calculate ctr per state
monthTempALL$allCTR <- monthallCTR
#head(monthTempALL, 10)
#dim(monthTempALL)



#plot by month
png(filename="ctrMonthByTemp.png", width=1200, height=1024, pointsize = 20 )
tempMonthCTRs <- monthTempALL$allCTR#c(monthTempALL$allCTR)#, highTempALL$highCTR, LowTempALL$lowCTR, MidTempALL$midCTR)
#head(tempMonthCTRs)
rownames=c(as.character(monthTempALL$month))#c("alle Temperaturen","> 28", "< -2","~9")
colors <- c( "cadetblue2","cadetblue3", "palegreen1", "palegreen2", "palegreen3", "firebrick1", "firebrick2", "firebrick3", "tan2", "tan3", "tan4" ,"cadetblue1")
ctrPlot<- barplot(tempMonthCTRs, col= colors, border="white", font.axis=2, beside=T,  xlab="months", ylab="CTR", font.lab=2, names.arg = as.character(monthTempALL$month))

par(mar=c(10.1, 4.1, 4.1, 5))
par(xpd=TRUE)
colortones <- c("Spring","Summer", "Fall","Winter")
colortonesLegend <- c("palegreen2","firebrick2", "tan3","cadetblue2")
legend(par("usr")[1],par("usr")[3], pch = 15, col = colortonesLegend, legend = colortones, xpd=TRUE, ncol = 1, title = "Season", xjust = -0.5, yjust = -1)
text(ctrPlot,tempMonthCTRs+0.1,labels=as.character(round(tempMonthCTRs, 2)))
title("CTR pro Monat - gesamte Kampagne")
dev.off()

#define function for shiny
ctrByMonthShiny <- function(xadjust, yadjust){
  tempMonthCTRs <- monthTempALL$allCTR#c(monthTempALL$allCTR)#, highTempALL$highCTR, LowTempALL$lowCTR, MidTempALL$midCTR)
  #head(tempMonthCTRs)
  rownames=c(as.character(monthTempALL$month))#c("alle Temperaturen","> 28", "< -2","~9")
  colors <- c( "cadetblue2","cadetblue3", "palegreen1", "palegreen2", "palegreen3", "firebrick1", "firebrick2", "firebrick3", "tan2", "tan3", "tan4" ,"cadetblue1")
  ctrPlot<- barplot(tempMonthCTRs, col= colors, border="white", font.axis=2, beside=T,  xlab="months", ylab="CTR", font.lab=2, names.arg = as.character(monthTempALL$month))
  par(mar=c(10.1, 4.1, 4.1, 5))
  par(xpd=TRUE)
  colortones <- c("Spring","Summer", "Fall","Winter")
  colortonesLegend <- c("palegreen2","firebrick2", "tan3","cadetblue2")
  legend(par("usr")[1],par("usr")[3], pch = 15, col = colortonesLegend, legend = colortones, xpd=TRUE, ncol = 1, title = "Season", xjust = xadjust, yjust = yadjust)
  text(ctrPlot,tempMonthCTRs+0.1,labels=as.character(round(tempMonthCTRs, 2)))
  title("CTR pro Monat - gesamte Kampagne")

}



# Korrelation Wert für gute Position
#group data by weeks:
PositionUnder3weeklygAdsWDR <- sqldf("SELECT COUNT(*), SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position_week, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE average_position <2.3")
weeklyCTR <- (PositionUnder3weeklygAdsWDR$totalClicks / PositionUnder3weeklygAdsWDR$totalImpressions)*100 # calculate ctr
weeklyAverageCosts <- PositionUnder3weeklygAdsWDR$costs/PositionUnder3weeklygAdsWDR$totalClicks # calculate costs average per week
PositionUnder3weeklygAdsWDR$weeklyCTR <- weeklyCTR
PositionUnder3weeklygAdsWDR$average_costs <- weeklyAverageCosts
dim(PositionUnder3weeklygAdsWDR)
head(PositionUnder3weeklygAdsWDR, 10)

# Korrelation Wert für mittlere Position
#group data by weeks:
PositionMedianweeklygAdsWDR <- sqldf("SELECT COUNT(*), SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position_week, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE average_position = 4")
weeklyCTR <- (PositionMedianweeklygAdsWDR$totalClicks / PositionMedianweeklygAdsWDR$totalImpressions)*100 # calculate ctr
weeklyAverageCosts <- PositionMedianweeklygAdsWDR$costs/PositionMedianweeklygAdsWDR$totalClicks # calculate costs average per week
PositionMedianweeklygAdsWDR$weeklyCTR <- weeklyCTR
PositionMedianweeklygAdsWDR$average_costs <- weeklyAverageCosts
dim(PositionMedianweeklygAdsWDR)
head(PositionMedianweeklygAdsWDR, 10)

# Korrelation Wert für unter Position
#group data by weeks:
PositionOver4weeklygAdsWDR <- sqldf("SELECT COUNT(*), SUM(clicks) as totalClicks, SUM(impressions) as totalImpressions,
 SUM(costs) as costs, AVG(average_position) as average_position_week, AVG(temp_min) as average_temp_min, AVG(temp_max) as average_temp_max,
 AVG(temp_average) as temp_average
 FROM gAdsWDR
 WHERE average_position > 5.5")
weeklyCTR <- (PositionOver4weeklygAdsWDR$totalClicks / PositionOver4weeklygAdsWDR$totalImpressions)*100 # calculate ctr
weeklyAverageCosts <- PositionOver4weeklygAdsWDR$costs/PositionOver4weeklygAdsWDR$totalClicks # calculate costs average per week
PositionOver4weeklygAdsWDR$weeklyCTR <- weeklyCTR
PositionOver4weeklygAdsWDR$average_costs <- weeklyAverageCosts
dim(PositionOver4weeklygAdsWDR)
head(PositionOver4weeklygAdsWDR, 10)


png(filename="ctrByPosition.png", width=768, height=1024, pointsize = 20 )
tempCTRs <- c(PositionUnder3weeklygAdsWDR$weeklyCTR, PositionMedianweeklygAdsWDR$weeklyCTR, PositionOver4weeklygAdsWDR$weeklyCTR)
rownames=c("2.3 und besser","4", "5.5 und schlechter")
colors <- c("orange","tomato2","turquoise3")
ctrPlot<- barplot(tempCTRs, col= colors, border="white", font.axis=2, beside=T, ylab="CTR",xlab="durchschnittliche Position", font.lab=2, names.arg = rownames)
#par(xpd=T, mar=par()$mar+c(2,2,2,4))
par(mar=c(10.1, 4.1, 4.1, 5))
par(xpd=TRUE)
#legend(par("usr")[1],par("usr")[3], pch = 15, col = colors, legend = rownames, xpd=TRUE, ncol = 1, title = "Positionen", xjust = -0.5, yjust = -1)
text(ctrPlot,tempCTRs+0.1,labels=as.character(round(tempCTRs, 2)))
title("CTR - Positionen")
dev.off()

generateCTRByPositionShiny <- function (titleName) {
  tempCTRs <- c(PositionUnder3weeklygAdsWDR$weeklyCTR, PositionMedianweeklygAdsWDR$weeklyCTR, PositionOver4weeklygAdsWDR$weeklyCTR)
  rownames=c("2.3 und besser","4", "5.5 und schlechter")
  colors <- c("orange","tomato2","turquoise3")
  ctrPlot<- barplot(tempCTRs, col= colors, border="white", font.axis=2, beside=T, ylab="CTR",xlab="durchschnittliche Position", font.lab=2, names.arg = rownames)
  par(mar=c(10.1, 4.1, 4.1, 5))
  par(xpd=TRUE)
  text(ctrPlot,tempCTRs+0.1,labels=as.character(round(tempCTRs, 2)))
  title(titleName)
}



#
uniLinRegClicksImpress <- lm(weeklyCTR~costs + temp_average, weeklygAdsWDR)
head(weeklygAdsWDR)
lapply(weeklygAdsWDR, class)
summary(uniLinRegClicksImpress)
uniLinRegClicksImpress


####################################
# Begründung der Progrnose in den Fragestellungen
####################################


sink() #stop logfile











####################################
# shiny Dashboard
####################################






# Define UI for app that draws a histogram ----
ui <- dashboardPage(skin = "purple",
  dashboardHeader(title = "gAds-Weather Dashboard"),
  dashboardSidebar(
    sidebarMenu(
      #sidebarSearchForm(textId = "searchText", buttonId = "searchButton", label = "Search..."),
      menuItem("Dashboard", tabName = "dashboard", icon = icon("dashboard")),
      menuItem("Geomap", tabName = "geomaps", icon = icon("th")),
      menuItem("Korrelation", tabName = "korrelation", icon = icon("table")),
      menuItem("GitLab Repo", icon = icon("file-code-o"),
           href = "https://gitlab.com/OllowainT/data-analysis-and-visualization") #adjust repo here
    )

  ),
  dashboardBody(
    tabItems(
      # First tab content
      tabItem(tabName = "geomaps",
        fluidRow(
          #should be the sidebar
          box(title="Options",solidHeader = TRUE,status="info", width=3,
            #dateRangeInput("selectDate", h4("Date range")),
            selectInput("selectGermanSort", label = h4("Select type of interest"),
            choices = list("CTR" = "stateCTR", "Clicks" = "totalClicks", "maximum Temperature" = "average_temp_max",
            "minimum Temperature" = "average_temp_min", "average Temperature" = "temp_average", "Impressions" = "totalImpressions",
            "Average Position" = "average_position", "Costs"= "costs"
            ),
            selected = "ctr")
          ),

          box(title="Map",solidHeader = TRUE,status="primary", color="navy",width=9,
            h3(textOutput("selsctedSortMap")),
            plotOutput("germanMap")
          )
        )
      ),

      # Second tab content
      tabItem(tabName = "dashboard",
        fluidRow(
          box(title="Overall Data Info",solidHeader = TRUE, width=12,
            valueBoxOutput("impressionBox"),
            valueBoxOutput("clicksBox"),
            valueBoxOutput("ctrBox"),
            valueBoxOutput("positionBox"),
            valueBoxOutput("cpcBox"),
            valueBoxOutput("costsBox")

          )
        ),
        fluidRow(
          column(width=12,
            box(title="Average Position",solidHeader = TRUE, width=2,status="info",
              plotOutput("boxplotPosition")
            ),
            box(title="Average Temp",solidHeader = TRUE, width=2,status="info",
              plotOutput("boxplotAverageTemp")
            ),
            box(title="CTR by Month",solidHeader = TRUE, width=5,status="info",
              plotOutput("ctrByMonth")
            ),
            box(title="CTR by extreme Position",solidHeader = TRUE, width=3,status="info",
              plotOutput("CTRByPositionShiny")
            )
          )
        ),
        fluidRow(
          box(title="Korrelation - weekly sorted",solidHeader = TRUE, width=7,status="info",
            plotOutput("weeklymatrix")
          ),
          box(title="Extreme Temperatures",solidHeader = TRUE, width=5,status="info",
            plotOutput("extremeTemps")
          )

      )
      ,
      fluidRow(
        box(title="Temperature Info",solidHeader = TRUE, width=12,
          valueBoxOutput("minTempBox"),
          valueBoxOutput("averageTempBox"),
          valueBoxOutput("maxTempBox")
        )
      )
    ),
    tabItem(tabName = "korrelation",
      fluidRow(
        #should be the sidebar
        box(title="Options",solidHeader = TRUE,status="info", width=3,
          selectInput("selectKorrelation", label = h4("Select Korrelation"),
          choices = list("dayly" = "day", "weekly" = "week", "monthly" = "month", "season" = "season"),
          selected = "weekly")
        ),

        box(title="Korrelationsmatrix",solidHeader = TRUE,status="primary", color="navy", width=9,
          plotOutput("korrelationsmatrix")
        )
      )
    )



  )
)
)
##########################################################
#
#
##########################################################
#Define server logic required to draw a histogram ----
server <- function(input, output) {
 output$selected_var <- renderText({
     "You have selected this"
   })

 output$boxplotPosition <- renderPlot({
   generateBoxplot1("tempAverageMaxOverall", gAdsWDR$average_position, "", "position", "")
 })

 output$boxplotCTR <- renderPlot({
   generateBoxplot1("averageCTR", gAdsWDR$ctr, "", "ctr", "")
 })

 output$boxplotAverageTemp <- renderPlot({
    generateBoxplot1("tempAverageOverall", gAdsWDR$temp_average, "", "temperature", "")
 })




 output$germanMap <- renderPlot({
   selected <- input$selectGermanSort
   data <- switch(input$selectGermanSort,
               "stateCTR" = germany$stateCTR,
               "totalClicks" = germany$totalClicks,
               "average_temp_max" = germany$average_temp_max,
               "average_temp_min" = germany$average_temp_min,
               "temp_average" = germany$temp_average,
               "totalImpressions" = germany$totalImpressions,
               "average_position" = germany$average_position,
               "costs" = germany$costs
   )
   generateMapShiny(data,  8, 0)
 })

 output$korrelationsmatrix <- renderPlot({
   selected <- input$selectKorrelation
   data <- switch(input$selectKorrelation,
               "day" = daygAdsWDR,
               "week" = weeklygAdsWDR,
               "month" = monthgAdsWDR,
               "season" = seasongAdsWDR
   )
   generateCorrelationMatrix(data)
 })

 output$weeklymatrix <- renderPlot({
   generateCorrelationMatrix(weeklygAdsWDR)
 })

 output$extremeTemps <- renderPlot({
   generateExtremeTemps(-0.1,-0.1)
 })

 output$ctrByMonth <- renderPlot({
    ctrByMonthShiny(-0.1,-0.1)
 })

 output$CTRByPositionShiny <- renderPlot({
   generateCTRByPositionShiny("")
 })


 output$selsctedSortMap <- renderText({
   paste("You have selected", input$selectGermanSort)
 })

# ValueBoxes:
 output$impressionBox <- renderValueBox({
   sumOfImpressions <- sum(gAdsWDR$impressions)
    valueBox(
      as.character(sumOfImpressions), "Impressions", icon = icon("globe-asia "),
      color = "orange"
    )
  })

  output$clicksBox <- renderValueBox({
    sumOfClicks <- sum(gAdsWDR$clicks)
     valueBox(
       as.character(sumOfClicks), "Clicks", icon = icon("mouse-pointer"),
       color = "yellow"
     )
   })

   output$ctrBox <- renderValueBox({
     averageCtr <- sum(gAdsWDR$clicks) / sum(gAdsWDR$impressions) * 100
      valueBox(
        paste0(as.character(round(averageCtr,3)) , " %"), "Average CTR", icon = icon("user-check"),
        color = "olive"
      )
    })

    output$averageTempBox <- renderValueBox({
      averageTemp <- mean(gAdsWDR$temp_average, na.rm = TRUE)
       valueBox(
         paste0(as.character(round(averageTemp,2)) , "\u00B0C"), "Average Temperature", icon = icon("thermometer-half "),
         color = "fuchsia"
       )
     })

     output$minTempBox <- renderValueBox({
       minTemp <- mean(gAdsWDR$temp_min, na.rm = TRUE)
        valueBox(
          paste0(as.character(round(minTemp,2)) , "\u00B0C"), "Average minimal Temperature", icon = icon("thermometer-empty"),
          color = "aqua"
        )
      })

      output$maxTempBox <- renderValueBox({
        maxTemp <- mean(gAdsWDR$temp_max, na.rm = TRUE)
         valueBox(
           paste0(as.character(round(maxTemp,2)) , "\u00B0C"), "Average maximal Temperature", icon = icon("thermometer-full "),
           color = "red"
         )
       })

       output$positionBox <- renderValueBox({
         avPosition <- mean(gAdsWDR$average_position, na.rm = TRUE)
          valueBox(
            as.character(round(avPosition,2)), "Average Position", icon = icon("poll-h "),
            color = "lime"
          )
        })

        output$cpcBox <- renderValueBox({
          avCpc <- mean(gAdsWDR$average_cpc, na.rm = TRUE)
           valueBox(
             paste0(as.character(round(avCpc,2)), "\u20ac"), "Average CPC", icon = icon("money-bill-alt "),
             color = "teal"
           )
         })

         output$costsBox <- renderValueBox({
           sumCosts<- sum(gAdsWDR$costs, na.rm = TRUE)
            valueBox(
              paste0(as.character(round(sumCosts,2)), "\u20ac"), "Overall Costs", icon = icon("file-invoice-dollar "),
              color = "maroon"
            )
          })



 output$distPlot <- renderPlot({

   x    <- faithful$waiting
   bins <- seq(min(x), max(x), length.out = input$bins + 1)

   hist(x, breaks = bins, col = "#75AADB", border = "orange",
        xlab = "Waiting time to next eruption (in mins)",
        main = "Histogram of waiting times")

   })

}
options = list(shiny.port = 5000)
shinyApp(ui, server, options = list(port = 5000))
