To let this program run just install all needed packages (list below) and run in the root directory "Rscript.exe proj_daemm.r" (Windows cmd).


After a short time all the plots are going to be generated.
As long as you dont abort the console run, a webserver (listening to "localhost:5000") will be available. 
This url opens the Shinydashboard.
The Output of the console ist logged in "analysis-output.txt"

This Project needs this Packages:
plyr,
sqldf,
ggplot2,
ggpubr,
corrplot,
gridExtra,
sp,
rgdal,
RColorBrewer,
maps,
mapdata,
shapefiles,
maptools,
raster,
tidyverse,
stringr,
viridis,
shiny,
shinydashboard